<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

// Route::get('/', [HomeController::class,'home'])->name('home');



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// //cara 1
// Route::get('/about', function () {
//     return view('aboutme');
// });

//cara 2
// Route::get('/about', 'HomeController@about')->name('about');

// Route::get('/about', 'HomeController@home')->name('home');


Route::get('/', [HomeController::class,'home'])->name('home');
Route::get('/about', [HomeController::class,'about'])->name('about');
Route::get('/contact', [HomeController::class,'contact'])->name('contact');
Route::get('/products', [HomeController::class,'products'])->name('products');


// Route::get('/contact', 'HomeController@contact')->name('contact');

// Route::get('/products', 'HomeController@products')->name('products');

